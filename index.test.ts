import { MyPromise } from "./MyPromise";

const runTest = (description: string, P: typeof Promise) => {
  describe(description, () => {
    it('uses then on new Promise', () => {
      return new P((resolve) => resolve('ok'))
        .then(result => expect(result).toEqual('ok'));
    });
    it('resolves a new Promise', () => {
      return expect(new P((resolve) => resolve('ok')))
        .resolves.toEqual('ok');
    });
    it('rejects a new Promise', () => {
      return expect(new P((resolve, reject) => reject('ko')))
        .rejects.toEqual('ko');
    });

    it('Chains then', () => {
      return new P((resolve) => resolve('ok'))
        .then(result => {
          expect(result).toEqual('ok');
          return 'ok2';
        })
        .then(result => {
          expect(result).toEqual('ok2');
          return 'ok3';
        })
        .then(result => {
          expect(result).toEqual('ok3');
        })
    });

    it('catches and then resolves', () => {
      return new P((resolve, reject) => reject('ko'))
        .catch(error => {
          expect(error).toEqual('ko');
          return 'ok';
        })
        .then(result => {
          expect(result).toEqual('ok');
        })
    });
    it('does not call then on error', () => {
      const thenSpy = jest.fn();
      const catchSpy = jest.fn();
      return new P((resolve, reject) => reject('ko'))
        .then(thenSpy)
        .catch(catchSpy)
        .then(() => {
          expect(thenSpy).not.toHaveBeenCalled();
          expect(catchSpy).toHaveBeenCalled();
        });
    });
    it('catches error thrown in executor', () => {
      const catchSpy = jest.fn();
      return new P((resolve, reject) => {throw 'ko'})
        .catch(catchSpy)
        .then(() => {
          expect(catchSpy).toHaveBeenCalledWith('ko');
        });
    });
    it('catches errors thrown in then', () => {
      const catchSpy = jest.fn();
      return new P((resolve) => resolve('ok'))
        .then(value => {throw 'ko'})  
        .catch(catchSpy)
        .then(() => {
          expect(catchSpy).toHaveBeenCalledWith('ko');
        });
    });
    it('catches errors thrown in catch', () => {
      const catchSpy = jest.fn();
      return new P((_, reject) => reject('ko'))
        .catch(() => {throw 'ko2'})
        .catch(catchSpy)
        .then(() => {
          expect(catchSpy).toHaveBeenCalledWith('ko2');
        });
    });
    it('can catch with then', () => {
      const catchSpy = jest.fn();
      const thenSpy = jest.fn();
      return new P((_, reject) => reject('ko'))
        .then(thenSpy, catchSpy)
        .then(() => {
          expect(thenSpy).not.toHaveBeenCalled();
          expect(catchSpy).toHaveBeenCalledWith('ko');
        });
    });
    it('supports await', async () => {
      expect(await new P(resolve => resolve('ok'))).toEqual('ok');
    })
    it('has a static resolve', async () => {
      expect(P.resolve('ok')).resolves.toEqual('ok');
    })
    it('has a static reject', async () => {
      expect(P.resolve('ok')).rejects.toEqual('ko');
    })
    it('resolves any object with a then function returned from then', () => {
      return P.resolve('ok')
        .then(() => ({then: (f) => f('ok2')}) as any)
        .then(result => {
          expect(result).toEqual('ok2');
        });
    })
    it('resolves promises returned from then', async () => {
      const thenSpy = jest.fn();
      return new P((resolve) => resolve('ok'))
        .then(() => P.resolve('ok2'))
        .then(result => {
          expect(result).toEqual('ok2');
        });
    })
    it('resolves multiple nested promises returned from then', async () => {
      const thenSpy = jest.fn();
      return new P((resolve) => resolve('ok'))
        .then(() => P.resolve(P.resolve('ok2')))
        .then(result => {
          expect(result).toEqual('ok2');
        });
    })

    it('resolves promises returned from executor', async () => {
      const thenSpy = jest.fn();
      return new P((resolve) => resolve(P.resolve('ok')))
        .then(result => {
          expect(result).toEqual('ok');
        });
    })

    it('resolves multiple nested promises returned from executor', async () => {
      const thenSpy = jest.fn();
      return new P((resolve) => resolve(P.resolve(P.resolve('ok'))))
        .then(result => {
          expect(result).toEqual('ok');
        });
    })
  })
}

runTest('Promise', Promise);
runTest('MyPromise', MyPromise as any);
