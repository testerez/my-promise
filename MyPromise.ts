interface Handler {
  onResolve?: (result) => any;
  onReject?: (result) => any;
}

type PromiseStatus = 'resolved' | 'rejected' | 'pending';

export class MyPromise {
  private queue = [] as Handler[];
  private status: PromiseStatus = 'pending';
  private result;
  private isProcessing;

  static resolve = result => new MyPromise(resolve => resolve(result));
  static reject = error => new MyPromise((_, reject) => reject(error));

  constructor(
    executor: (resolve, reject) => void
  ) {
    try {
      executor(
        value => this.setStatus('resolved', value),
        error => this.setStatus('rejected', error),
      );
    } catch (error) {
      this.setStatus('rejected', error);
    }
  }

  private setStatus = (status: PromiseStatus, result: any) => {
    this.result = result;
    this.status = status;
    while (this.queue.length) {
      this.processHanldlers();
    }
  }

  private processHanldlers = () => {
    if (this.isProcessing || !this.queue.length) {
      return;
    }
    this.isProcessing = true;
    const handler = this.queue.shift()!;
    try {
      if (this.status === 'resolved' && handler.onResolve) {
        this.result = handler.onResolve(this.result);
      } else if (this.status === 'rejected' && handler.onReject) {
        this.result = handler.onReject(this.result);
        this.status = 'resolved';
      }
    } catch (error) {
      this.status = 'rejected';
      this.result = error;
    }

    if (this.result && (typeof this.result.then === 'function')) {
      this.result.then(
        result => {
          this.isProcessing = false;
          this.result = result;
          this.processHanldlers();
        },
        error => {
          this.isProcessing = false;
          this.result = error;
          this.status = 'rejected';
          this.processHanldlers();
        }
      )
    } else {
      this.isProcessing = false;
      this.processHanldlers();
    }
  }

  private addHandler = (handler: Handler) => {
    this.queue.push(handler);
    if (this.status !== 'pending') {
      this.processHanldlers();
    }
    return this;
  }

  then = (onResolve, onReject?) => this.addHandler({ onResolve, onReject });
  catch = (onReject) => this.addHandler({ onReject });
}
